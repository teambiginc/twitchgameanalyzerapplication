package request;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class TwitchRequest {

	private String response;
	private String url;
	
	public TwitchRequest(String url)
	{
		this.url = url;
	}
	
	public void execute()
	{
		StringBuffer response = new StringBuffer();
		try
		{	
			/*"https://api.twitch.tv/kraken/streams?game=League+of+Legends"*/
				URL url = new URL(this.url);
		        HttpURLConnection con = (HttpURLConnection) url.openConnection(); 
		        con.setRequestMethod("GET");
		        
		        if (con.getResponseCode() == HttpURLConnection.HTTP_OK) { // success
		        	String inputLine;
		        	BufferedReader in = new BufferedReader(new InputStreamReader(
		                    con.getInputStream()));
		
		            while ((inputLine = in.readLine()) != null) {
		                response.append(inputLine);
		            }
		            in.close();

		            System.out.println(response.toString());
		        } else {
		            System.out.println("GET request not worked");
		        }
		} catch (Exception e)
		{
			
		}	
				this.response = response.toString();
				
			}





		
	public String getResponse() {
		return this.response;
	}
}
