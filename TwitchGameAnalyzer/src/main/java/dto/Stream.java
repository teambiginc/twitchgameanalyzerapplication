package dto;

public class Stream {

	private int id;
	private String game;
	private Channel channel = new Channel();
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getGame() {
		return game;
	}
	public void setGame(String game) {
		this.game = game;
	}
	public Channel getChannel() {
		return channel;
	}
	public void setChannel(Channel channel) {
		this.channel = channel;
	}
	

	@Override
	public String toString() {
		return "ID: " + id + ", Game: "+ game + ", Channel: "+ channel.toString();
	}
}
