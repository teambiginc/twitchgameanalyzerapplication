package dto;

public class Channel {

	private String status;
	private String name;
	private String displayName;
	
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDisplayName() {
		return displayName;
	}
	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "Status: " + status + ", Name: " + name + "Display Name: " + displayName;
	}
}
