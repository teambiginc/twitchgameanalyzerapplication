package rest;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import dao.StreamDAO;
import dto.Stream;

@Path("/Twitch")
public class TwitchEndpoint {

	@GET
	@Path("getTwitchData")
	@Produces(MediaType.APPLICATION_JSON)
	public String getTwitchData()
	{
		StreamDAO dao = new StreamDAO();
		List<Stream> list = dao.getStreamList();
		for(Stream stream : list)
		{
			System.out.println(stream.toString());
		}
		return "Test:";
	}
}
