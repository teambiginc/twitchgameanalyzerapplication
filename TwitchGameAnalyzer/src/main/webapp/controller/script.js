// script.js

    // create the module and name it scotchApp
        // also include ngRoute for all our routing needs
    var twitchGameAnalyzerApp = angular.module('TwitchGameAnalyzerApp', ['ngRoute']);
    var riotKey = "aadae54b-38b0-4a38-878e-c6aad6d0e378";
    var topPlayersData = [];
    function Streamer(name) {
        this.name = name;
        this.actualGameLength = "Actually not In game";
        this.playerID = "0";
        //this.lastName = last;
        //this.age = age;
        //this.eyeColor = eye;
    }
    function RequestHTTPData(funcPtr, type, link, count)
    {
    	console.log(count + ":   "+ link);
    	  var xhttp = new XMLHttpRequest();
    	  xhttp.open(type, link, true);
    	  xhttp.send(); 
    	  xhttp.onreadystatechange = function() {
    	  if (xhttp.readyState == 4 && xhttp.status == 200) {
    		  funcPtr(xhttp, type, link, count);
    	  }
    	  }
    }

    // create the controller and inject Angular's $scope
    twitchGameAnalyzerApp.controller('mainController', function($scope, $http) {
    	  Twitch.init({clientId: '9999'});
    	var data, i; 
    	var topPlayersGlobal;
    	$scope.testData = "None!";
    	$scope.topPlayers = "None";
    	
    	$scope.getTestData = function()
    	{
    		console.log("getTestData");
    		$http.get("api/Twitch/getTwitchData").success(function(data){
    			console.log("getTestData success");
    			$scope.myTestData = data;
    		});
    	}
    	
    	$scope.getSummonerByName = function(xhttp, type, link, count)
    	   {
    		   myData = JSON.parse(xhttp.response);
    		 for(property in myData)
    			 {
    			   topPlayersData[count].playerID = myData[property].id;
    			 } 
    			  
    			$scope.testData = myData;
    			$scope.$apply();
    	   }
    	$scope.getGameTimeBySummonerID = function(xhttp, type, link, count)
 	   {
 		   myData = JSON.parse(xhttp.response);
 		   console.log(myData);
 		   console.log("getgametimebysummonerid");
 		  topPlayersData[count].actualGameLength = myData.gameLength;
 			$scope.testData = myData;
 			$scope.$apply();
 	   }
    	$scope.refreshButton = function()
    	{
    		$scope.topPlayers = topPlayersData;
    		
    	}
    	$scope.getTopPlayersData = function(){
    		var list = [];
    		var xhrs = new XMLHttpRequest();
    		xhrs.onreadystatechange = function() {
    		    if (xhrs.readyState == 4 && xhrs.status == 200) {
    		    	data = JSON.parse(xhrs.response);
    	    		for(i=0; i<data.streams.length;i++){	
    	    			var streamer = new Streamer(data.streams[i].channel.display_name);
    	    			list.push(streamer);
    	    		}  
    	    		topPlayersData = list;	
    		    }
    		};
    		xhrs.open("GET", "https://api.twitch.tv/kraken/streams?game=League+of+Legends", true);
    		xhrs.send();
    		
    		
    	}
    	$scope.getGameTime = function()
    	{
    		var index = 0;
    		for(index=0;index<topPlayersData.length;index++)
			{
    			if(angular.equals(topPlayersData[index].playerID, "0") == false)
    				{
    				console.log(topPlayersData[index].name);
    		RequestHTTPData($scope.getGameTimeBySummonerID, "GET", "https://na.api.pvp.net/observer-mode/rest/consumer/getSpectatorGameInfo/NA1/"+ topPlayersData[index].playerID +"?api_key="+riotKey, index);	
			}
			}
    	}
    	$scope.getTopPlayers = function(){
		var index = 0;
    		for(index=0;index<topPlayersData.length;index++)
    			{
    				switch(topPlayersData[index].name)
    				{
    				case "SirhcEz":		
    									RequestHTTPData($scope.getSummonerByName, "GET", "https://na.api.pvp.net/api/lol/na/v1.4/summoner/by-name/mistersirhcez?api_key="+riotKey, index);	
    									break;
    				case "MushIsGosu":	RequestHTTPData($scope.getSummonerByName, "GET", "https://na.api.pvp.net/api/lol/na/v1.4/summoner/by-name/hiimgosu?api_key="+riotKey, index);
    									break;
    									
    				case "Valkrin": 	RequestHTTPData($scope.getSummonerByName, "GET", "https://na.api.pvp.net/api/lol/na/v1.4/summoner/by-name/chewyisgod?api_key="+riotKey, index);
										break;
    				
    				case "Voyboy": 		RequestHTTPData($scope.getSummonerByName, "GET", "https://na.api.pvp.net/api/lol/na/v1.4/summoner/by-name/voyboy?api_key="+riotKey, index);
										break;
					
    				case "Solaaaa": 	RequestHTTPData($scope.getSummonerByName, "GET", "https://euw.api.pvp.net/api/lol/euw/v1.4/summoner/by-name/spinsola?api_key="+riotKey, index);
										break;
										
    				case "imaqtpie":
    									RequestHTTPData($scope.getSummonerByName, "GET", "https://na.api.pvp.net/api/lol/na/v1.4/summoner/by-name/imaqtpie?api_key="+riotKey, index);
										break;
    				case "Chawyy": 		RequestHTTPData($scope.getSummonerByName, "GET", "https://na.api.pvp.net/api/lol/na/v1.4/summoner/by-name/Chawyy?api_key="+riotKey, index);
										break;
    				case "Nervarien": 	RequestHTTPData($scope.getSummonerByName, "GET", "https://na.api.pvp.net/api/lol/na/v1.4/summoner/by-name/Nervarien?api_key="+riotKey, index);
										break;
    				}
    			}
    			$scope.topPlayers = topPlayersData;
    	}
    });
